// deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(csvFilePathDeliveries)
.then((deliveryData)=>{
    
    const result = EconomySuperOver(deliveryData);
    fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/9-bowler-with-the-best-economy-in-super-overs.json', JSON.stringify(result), err => { 
        if(err)
        console.log(err);
        else
        console.log("Success");
    });
    
});

// Find the bowler with the best economy in super overs

function EconomySuperOver(deliveryData){
    const result = deliveryData.filter((everyBall) =>{
        return everyBall.is_super_over !== "0";
    }).reduce((accumulater, currentData)=>{
        
        if(accumulater[currentData.bowler]){

            accumulater[currentData.bowler].runs += Number(currentData.total_runs);
            accumulater[currentData.bowler].balls += 1;
            accumulater[currentData.bowler].economy = (accumulater[currentData.bowler].runs/(accumulater[currentData.bowler].balls/6)).toFixed(2);

        }else{

            accumulater[currentData.bowler] = {};
            accumulater[currentData.bowler].runs = Number(currentData.total_runs);
            accumulater[currentData.bowler].balls = 1;

        }
        return accumulater;
        
    },{});

    const economicOfBolwler =Object.entries(result).reduce((accumulater, currentData)=>{
        accumulater[currentData[0]]=currentData[1].economy;
        return accumulater;
    },{});
        const bestOfBowler = Object.fromEntries(Object.entries(economicOfBolwler).sort(([a],[b])=>[b][1]-[a][1]).slice(0,1)
    );
    return bestOfBowler;
}