// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.


const csv = require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePath = path.resolve(__dirname, '../data/matches.csv');


csv()
    .fromFile(csvFilePath)
    .then((jsonObject) => {
        const result = matchesPerYear(jsonObject);
        fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/1-matches-per-year.json', JSON.stringify(result), err => {
            if (err)
                console.log(err);
            else
                console.log("Success");
        });
    });


// Number of matches played per year for all the years in IPL.

function matchesPerYear(matches) {

    return matches.reduce((matchesPlayed, currentMatch) => {

        matchesPlayed[currentMatch.season] ? matchesPlayed[currentMatch.season]++ : matchesPlayed[currentMatch.season] = 1;

        return matchesPlayed;

    }, {});

}