// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.


const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePath = path.resolve(__dirname, '../data/matches.csv');



csv()
.fromFile(csvFilePath)
.then((jsonObject)=>{
    const result = matchesOwnPerYear(jsonObject);
    fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/2-matches-won-per-team-per-year.json', JSON.stringify(result), err => { 
        if(err)
        console.log(err);
        else
        console.log("Success");
    });
});


//  Number of matches won per team per year in IPL.

function matchesOwnPerYear(matches){

    const winCount = matches.reduce((accumulator, currentMatch) => {

    if (!(currentMatch['season'] in accumulator)) {
        accumulator[currentMatch['season']] = {};
    }
    if (!(currentMatch['winner'] in accumulator[currentMatch['season']])) {
        accumulator[currentMatch['season']][currentMatch['winner']] = 1;
    }

    accumulator[currentMatch['season']][currentMatch['winner']] += 1;

    return accumulator;
    }, {});
return winCount
    //console.log(winCount);

}