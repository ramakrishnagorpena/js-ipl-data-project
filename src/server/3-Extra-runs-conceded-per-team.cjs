// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.

// deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(csvFilePathMatches)
.then((matchData)=>{
    csv()
    .fromFile(csvFilePathDeliveries)
    .then((deliveryData)=>{
        
        const result = extraRunsConcededPerTeam(matchData, deliveryData);
        fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/3-Extra-runs-conceded-per-team.json', JSON.stringify(result), err => { 
            if(err)
            console.log(err);
            else
            console.log("Success");
        });
        
    })
    
});


function extraRunsConcededPerTeam(matchData, deliveryData){

    const matchesId = matchData.filter((matches)=>{
       return  matches.season == "2016";
    }).map(match=>{
        return match.id
    });

    let result= deliveryData.reduce((team, matchesData)=> {

        if(matchesId.includes(matchesData.match_id)){

            if(!team[matchesData.bowling_team]){
                team[matchesData.bowling_team] = Number(matchesData.extra_runs);
            }else{
                team[matchesData.bowling_team] += Number(matchesData.extra_runs);
            }

        }
        return team;

    },{})

   return result;
}