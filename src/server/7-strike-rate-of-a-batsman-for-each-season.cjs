// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.

// deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(csvFilePathMatches)
.then((matchData)=>{
    csv()
    .fromFile(csvFilePathDeliveries)
    .then((deliveryData)=>{
        
        const result = strikeRateOfBatsman(matchData, deliveryData);
        fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/7-strike-rate-of-a-batsman-for-each-season.json', JSON.stringify(result), err => { 
            if(err)
            console.log(err);
            else
            console.log("Success");
        });
        
    })
    
});

// Find the strike rate of a batsman for each season.

function strikeRateOfBatsman(matchData, deliveryData){

   const seasons = matchData.reduce((team, matchesData) => { team[matchesData.id] = matchesData.season;
    return team;
   },{});
//    console.log(seasons);
   const playerDetail = deliveryData.reduce((player, deliveryData) => {
    let season = seasons[deliveryData.match_id];
    if(player[deliveryData.batsman]&&player[deliveryData.batsman][season]){

        player[deliveryData.batsman][season].totalRuns += parseInt(deliveryData.total_runs);
        player[deliveryData.batsman][season].totalBallsFaced += 1;

    }else{
        player[deliveryData.batsman] = {};
        player[deliveryData.batsman][season] = {};
        player[deliveryData.batsman][season].totalRuns = parseInt(deliveryData.total_runs);
        player[deliveryData.batsman][season].totalBallsFaced = 1;
    }
    return player;
   },{});

   const data = {}
   Object.keys(playerDetail).map((batsman) => {
    let strike = {};
    Object.keys(playerDetail[batsman]).map(year => {
        let datas = (playerDetail[batsman][year].totalRuns/playerDetail[batsman][year].totalBallsFaced)*100;
        strike[year] = datas.toFixed(2);
    })
     data[batsman] = strike;
    });
    return data;
}

