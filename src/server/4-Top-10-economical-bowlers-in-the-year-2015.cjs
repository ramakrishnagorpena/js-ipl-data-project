// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.

// deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePathMatches = path.resolve(__dirname, '../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(csvFilePathMatches)
.then((matchData)=>{
    csv()
    .fromFile(csvFilePathDeliveries)
    .then((deliveryData)=>{
        
        const result = top10EconomicalBowlers(matchData, deliveryData);
        fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/4-Top-10-economical-bowlers-in-the-year.json', JSON.stringify(result), err => { 
            if(err)
            console.log(err);
            else
            console.log("Success");
        });
        
    })
    
});

// Top 10 economical bowlers in the year 2015

function top10EconomicalBowlers(matchData, deliveryData){
    
    const matchesId = matchData.filter((matches)=>{
        return  matches.season === "2015";
    }).map(match=>{
         return match.id
    });
 
    const economicBallScore= deliveryData.reduce((team, currentData)=> {
 
        if(matchesId.includes(currentData.match_id)){
            if(team[currentData.bowler]){

                let balls = Number(currentData.ball) <= 6 ? 1 : 0;
                team[currentData.bowler]["runs"] += Number(currentData.batsman_runs);
                team[currentData.bowler]["balls"] += balls;
                team[currentData.bowler]["economy"] = team[currentData.bowler]["runs"] / (team[currentData.bowler]["balls"] / 6);

            }else{

                team[currentData.bowler] = {};
                team[currentData.bowler]["runs"] = Number(currentData.total_runs);
                team[currentData.bowler]["balls"] = 1;

            }
        }
        return team;
    }, {});
 
    const economicBowlers = Object.entries(economicBallScore).reduce((accumulater, CurrentBowler) =>{
        accumulater[CurrentBowler[0]] = CurrentBowler[1].economy;
        return accumulater;
    },{});
    // console.log(economicBowlers);

    const result = Object.fromEntries(Object.entries(economicBowlers).sort(([a],[b]) => [a][1] - [b][1]).slice(0, 10));
    return result;
}