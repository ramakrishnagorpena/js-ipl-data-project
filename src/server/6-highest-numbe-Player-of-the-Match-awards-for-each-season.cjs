// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.


const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePath = path.resolve(__dirname, '../data/matches.csv');



csv()
.fromFile(csvFilePath)
.then((matches)=>{

    const result = highestnumberofPlayeroftheMatchawards(matches);
    fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/6-highest-numbe-Player-of-the-Match-awards-for-each-season.json', JSON.stringify(result), err => { 
        if(err)
        console.log(err);
        else
        console.log("Success");
    });
});


// Find a player who has won the highest number of Player of the Match awards for each season

function highestnumberofPlayeroftheMatchawards(matchData){

    let result = matchData.reduce((team, matchesData)=>{
        if(team[matchesData.season]){
        
            if(team[matchesData.season][matchesData.player_of_match]){
                team[matchesData.season][matchesData.player_of_match]++ ;
            }else{
                team[matchesData.season][matchesData.player_of_match] = 1;
            }
 
        }else{
            team[matchesData.season] = {};
        }
        return team;
        }, {});

        const highestPlayerOfMatchAward = Object.entries(result).reduce((accumulator, everyYear) =>{
        let year = Object.entries(everyYear[1]).sort((a,b)=>b[1]-a[1]).slice(0,1)
        accumulator[everyYear[0]]=year[0]; 
        return accumulator;
    }, {});
    return highestPlayerOfMatchAward;

}