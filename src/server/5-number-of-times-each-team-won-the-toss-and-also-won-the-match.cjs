// matches.csv contains details related to the match such as location, contesting teams, umpires, results, etc.


const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePath = path.resolve(__dirname, '../data/matches.csv');



csv()
.fromFile(csvFilePath)
.then((machObject)=>{
    // console.log(jsonObject);
    const result = teamWonTossAndMach(machObject);
    fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/5-number-of-times-each-team-won-the-toss-and-also-won-the-match.json', JSON.stringify(result), err => { 
        if(err)
        console.log(err);
        else
        console.log("Success");
    });
});


// Find the number of times each team won the toss and also won the match

function teamWonTossAndMach(matches){

    let result = matches.reduce((team, matchesaData)=>{
        
        if(team[matchesaData.toss_winner]){

            if(matchesaData.toss_winner === matchesaData.winner){
                team[matchesaData.toss_winner] = team[matchesaData.toss_winner]+1;
            }

        }else{

            if(matchesaData.toss_winner === matchesaData.winner){
                team[matchesaData.toss_winner]=1;
            }

        }
        return team;
            
    },{}) 
    return result;

}