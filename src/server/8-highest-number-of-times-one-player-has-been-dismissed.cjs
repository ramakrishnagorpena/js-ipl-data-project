// deliveries.csv is the ball-by-ball data of all the IPL matches including data of the batting team, batsman, bowler, non-striker, runs scored, etc.

const csv=require('csvtojson');
const fs = require("fs");
const path = require('path');

const csvFilePathDeliveries = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(csvFilePathDeliveries)
.then((deliveryData)=>{
    
    const result = dismissedPlayer(deliveryData);
     fs.writeFile('/home/lenovo/Desktop/IPL_Project/js-ipl-data-project/src/public/output/8-highest-number-of-times-one-player-has-been-dismissed.json', JSON.stringify(result), err => { 
        if(err)
        console.log(err);
        else
        console.log("Success");
    });
    
});

// Find the highest number of times one player has been dismissed by another player

function dismissedPlayer(deliveryData){
    const result = deliveryData.reduce((accumulater,currentData) =>{

        if(!(currentData.player_dismissed === "")){
            if(accumulater[currentData.batsman]){

                if( accumulater[currentData.batsman][currentData.bowler]){
                    accumulater[currentData.batsman][currentData.bowler]++;
                }else{
                    accumulater[currentData.batsman][currentData.bowler] = 1;
                }

            }else{

                accumulater[currentData.batsman] ={}; 
                if(accumulater[currentData.batsman][currentData.bowler]){
                    accumulater[currentData.batsman][currentData.bowler]++;
                }else{
                    accumulater[currentData.batsman][currentData.bowler]=1;
                }

            }
        }
        return accumulater;
    }, {});

    let maxDismissed = 0;
    let maxDismissedPlayers = {};

    Object.entries(result).map((batsman) => {
        const dismissalbatsman =Object.entries(batsman[1]).sort(([a],[b])=>[b][1]-[a][1]).slice(0,1);

            if(dismissalbatsman[0][1]>maxDismissed)
            {
                maxDismissed = dismissalbatsman[0][1];
                let tempObj = {};
                tempObj[batsman[0]]=dismissalbatsman[0];
                maxDismissedPlayers = tempObj;
            }
        });
    return maxDismissedPlayers;
}
// console.log(dismissedPlayer);